import React, { Component } from 'react';
import { connect } from 'react-redux';
import { updateText, incrementCounter, decrementCounter } from '../../redux/actions/ui';
import App from './App';

class AppContainer extends Component {
  render() {
    return (
      <div>
        <App
          onButtonClick={this.props.updateText}
          onDecrementClick={this.props.decrementCounter}
          onIncrementClick={this.props.incrementCounter}
          counter={this.props.ui.counter}
          didUpdate={this.props.ui.didUpdate}
        />
      </div>
    );
  }
}
// map STORE to props
const mapStateToProps = (state) => {
  return {
    ui: state.get('ui'),
  };
};

// Map actions to props
const mapDispatchToProps = {
  updateText,
  incrementCounter,
  decrementCounter
};

export default connect(mapStateToProps, mapDispatchToProps)(AppContainer);
