import * as actions from '../constants';

export function updateText() {
  return {
    type: actions.UPDATE_TEXT,
  };
}

export function incrementCounter() {
  return {
    type: actions.INCREMENT_COUNTER,
  };
}

export function decrementCounter() {
  return {
    type: actions.DECREMENT_COUNTER,
  };
}
