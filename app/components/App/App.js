import React from 'react';
import { Switch, Route } from 'react-router-dom';
import './style.scss';

const App = ({ onButtonClick, onIncrementClick, onDecrementClick, counter, didUpdate}) => (
  <div className="app-wrapper">
    <Switch>
      <Route exact path="/" component={() => <div>homepage</div>} />
    </Switch>
    <button onClick={onButtonClick}>
      Click me!
    </button>

      {didUpdate && (
        <div>
          <button onClick={onIncrementClick}>
            +
          </button>
          <button onClick={onDecrementClick}>
            -
          </button>
          <div>Counter: {counter}</div>
        </div>
      )}

  </div>
);

export default App;
